/*Menu Dropdown Onhover Js*/
jQuery(document).ready(function () {
    jQuery('.nav li.dropdown').hover(function () {
        jQuery(this).addClass('open');
    }, function () {
        jQuery(this).removeClass('open');
    });

});

/* Page Scroll onclick button Js */
jQuery(document).ready(function () {

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scrollup').fadeIn();
        } else {
            jQuery('.scrollup').fadeOut();
        }
    });

    jQuery('.scrollup').click(function () {
        jQuery("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });


    // step one


    jQuery('#gform_next_button_1_7').click(function () {

        var guid = jQuery("input[name='input_33']").val();

        dataLayer.push(
            {
                "event": 'quiz_event',
                "entryID": guid,
                "eventName":'step1_bankcardswallowed'
            }
        );

    });

    // step two -- gform_next_button_1_20

    jQuery('#gform_next_button_1_20').click(function () {

        var guid = jQuery("input[name='input_25']").val();

        dataLayer.push(
            {
                "event": 'quiz_event',
                "entryID": guid,
                "eventName":'step2_financialexpert'
            }
        );

    });


    // step three -- gform_next_button_1_21


    jQuery('#gform_next_button_1_21').click(function () {

        var guid = jQuery("input[name='input_26']").val();

        dataLayer.push(
            {
                "event": 'quiz_event',
                "entryID": guid,
                "eventName":'step3_hospitalization'
            }
        );

    });


    // step four -- gform_next_button_1_22

    jQuery('#gform_next_button_1_22').click(function () {

        var guid = jQuery("input[name='input_27']").val();

        dataLayer.push(
            {
                "event": 'quiz_event',
                "entryID": guid,
                "eventName":'step4_credit'
            }
        );

    });


    // step five -- gform_next_button_1_23


    jQuery('#gform_next_button_1_23').click(function () {

        var guid = jQuery("input[name='input_33']").val();

        dataLayer.push(
            {
                "event": 'quiz_event',
                "entryID": guid,
                "eventName":'step51_failed'
            }
        );

    });



    // submit button
    jQuery('#gform_submit_button_1').click(function () {

        var guid = jQuery('#gform_next_button_1_22').val();
        dataLayer.push(
            {
                "event": 'quiz_event',
                "entryID": guid,
                "event":'step6_entrycompleted'
            }
        );

    });


})
;

/* Tooltips on anchor Js */
jQuery(function () {
    jQuery('[data-toggle="tooltip"]').tooltip()
})


