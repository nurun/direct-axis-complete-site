/*Menu Dropdown Onhover Js*/
jQuery(document).ready(function () {
    jQuery('.nav li.dropdown').hover(function () {
        jQuery(this).addClass('open');
    }, function () {
        jQuery(this).removeClass('open');
    });

});

/* Page Scroll onclick button Js */
jQuery(document).ready(function () {

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scrollup').fadeIn();
        } else {
            jQuery('.scrollup').fadeOut();
        }
    });

    jQuery('.scrollup').click(function () {
        jQuery("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });


    // step one


    jQuery('#gform_next_button_1_7').click(function () {

        var guid = jQuery("input[name='input_33']").val();

        dataLayer.push(
            {
                "entryID": guid,
                "event":'quiz_event',
                "eventName":'Quiz_Details_Submitted'
            }
        );

    });


    // step two -- gform_next_button_1_20


    jQuery('#gform_next_button_1_20').click(function () {

        var guid = jQuery("input[name='input_33']").val();

        dataLayer.push(
            {
                "entryID": guid,
                "event":'quiz_event',
                "eventName":'Quiz_Step1_Submitted'
            }
        );

    });


    // step three -- gform_next_button_1_21


    jQuery('#gform_next_button_1_21').click(function () {

        var guid = jQuery("input[name='input_33']").val();

        dataLayer.push(
            {
                "entryID": guid,
                "event":'quiz_event',
                "eventName":'Quiz_Step2_Submitted'
            }
        );

    });


    // step four -- gform_next_button_1_22

    jQuery('#gform_next_button_1_22').click(function () {

        var guid = jQuery("input[name='input_33']").val();

        dataLayer.push(
            {
                "entryID": guid,
                "event":'quiz_event',
                "eventName":'Quiz_Step3_Submitted'
            }
        );

    });


    // step five -- gform_next_button_1_23


    jQuery('#gform_next_button_1_23').click(function () {

        var guid = jQuery("input[name='input_33']").val();

        dataLayer.push(
            {
                "entryID": guid,
                "event":'quiz_event',
                "eventName":'Quiz_Step4_Submitted'
            }
        );

    });



    // submit button
    jQuery('#gform_submit_button_1').click(function () {

        var guid = jQuery("input[name='input_33']").val();
        dataLayer.push(
            {
                "entryID": guid,
                "event":'quiz_event',
                "eventName":'Quiz_Complete'
            }
        );

    });


})
;

/* Tooltips on anchor Js */
jQuery(function () {
    jQuery('[data-toggle="tooltip"]').tooltip()
})


