<?php
/**
 * blueamber functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package getfit
 */

if (!function_exists('getfit_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function getfit_setup()
    {

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'main' => __('Primary', 'getfit'),
        ));

    }
endif;

add_action('after_setup_theme', 'getfit_setup');


/**
 * Enqueue scripts and styles.
 */
function getfit_scripts()
{


    wp_enqueue_style('getfit-style', get_stylesheet_uri());
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.css');
    wp_enqueue_style('getfit-custom', get_template_directory_uri() . '/assets/css/custom.css?1');


    wp_enqueue_script('jquery');
    wp_enqueue_script('getfit-bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
    wp_enqueue_script('getfit-customjs', get_template_directory_uri() . '/assets/js/custom.js', array(), '1', true);

}

add_action('wp_enqueue_scripts', 'getfit_scripts');


add_theme_support('post-thumbnails');


include 'mobile.php';

//[infographic]
function infoprahic_func($atts)
{
    $detectinfo = new Mobile_Detect();
    $post_id = get_the_ID();
    if (get_field('mobile_infographic', $post_id)) {
        if ($detectinfo->isMobile()) {
            return '<a href="http://www.facebook.com/sharer.php?u='.get_permalink().'" target="_blank"><img alt="'.get_the_title().'" src="' . get_field('mobile_infographic', $post_id) . '" class=""/></a>';
        } else {
            return '<a href="http://www.facebook.com/sharer.php?u='.get_permalink().'" target="_blank"><img alt="'.get_the_title().'" src="' . get_field('desktop_infographic', $post_id) . '" class=""/></a>';
        }

    } else {
        return '';
    }
}


add_shortcode('infographic', 'infoprahic_func');