<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package blueamber
 */

?>
<!-- Footer Section -->
<footer class="footer-sidebar">
    <!-- Footer Widgets -->
    <div class="container">
        <div class="row" style="display:block !important">
            <div class="col-md-12">
                <div class="socialmedia" style="text-align: center;">
                    <p>Find us on social media</p>
                    <a href="https://www.facebook.com/DirectAxisSA/" target="_blank" class="sociallink"><img src="<?php echo get_template_directory_uri() ?>/assets/images/facebook.png"  alt="Direct Axis"></a>
                    <a href="https://twitter.com/DirectAxis" target="_blank" class="sociallink"><img src="<?php echo get_template_directory_uri() ?>/assets/images/twitter.png" alt="Direct Axis"></a>
                    <a href="https://www.linkedin.com/company/direct-axis" target="_blank" class="sociallink"><img src="<?php echo get_template_directory_uri() ?>/assets/images/linkedin.png"  alt="Direct Axis"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /End of Footer Widgets -->

    <!-- Copyrights -->
    <div class="site-info">
        <div class="container" >
            <div class="row" style="display:block !important">
                <div class="col-md-12">
                    <div class="disclaimerlogo">
                        <a href="https://www.directaxis.co.za/" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/assets/images/DA.png" class="footerlogo" alt="Direct Axis"></a>
                    </div>
                    <div class="disclaimer">
                        Your direct connection to Financial Services. Direct Axis SA (Pty) Ltd (Registration no 1995/006077/07) is an authorised Financial Services Provider (FSP 5 and FSP 7249) © Copyright 2018<br>
                        Direct Axis, a Business unit of First Rand Bank Limited<br>
                        <a href="<?php echo get_template_directory_uri() ?>/assets/content/privacy_policy.pdf" target="_blank">Privacy Policy</a> | <a href="https://www.directaxis.co.za/about/legal-documents" target="_blank">Legal Documents</a> | <a href="/terms-conditions/" target="_blank">Terms & Conditions</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyrights -->

</footer>
<!-- /End of Footer Section -->

<?php wp_footer(); ?>

</body>
</html>
