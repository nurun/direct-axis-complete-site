<?php
get_header();
//the_post();
?>
<?php
/*
 * Hero Banner
 * */
get_template_part('template-parts/content-hero-banner-homepage');
?>
    <!-- Page Title -->
    <!-- End of Page Title -->

    <div class="clearfix"></div>
    <section>
        <div class="container">
            <div class="row" style="display:block !important">

                <!--Blog Detail-->
                <div class="col-md-10 col-md-offset-1 col-xs-12">

                    <div class="site-content">
                        <?php the_title('<h1>', '</h1>'); ?>
                        <?php
                        if (have_posts()) :
                            // Start the Loop.
                            while (have_posts()) : the_post();

                                ?>
                                <article class="post">
                                    <div class="entry-content">
                                        <?php the_content(); ?>
                                    </div>
                                </article>

                            <?php
                            endwhile; endif;
                        ?>

                    </div>
                </div>
                <!--/End of Blog Detail-->

            </div>
        </div>
    </section>

    <!--/Start of Post blocks-->
    <div class="row busiprof-features-content" style="background-color: lightgrey; float: left; width: 100%; margin:0">

    <div class="blocks">
        <?php
        global $post;
        $current_post = $post; // remember the current post
        $counter = 0;
        while ($counter < 4):
            $post = get_previous_post(); // this uses $post->ID
            setup_postdata($post);

            if ($post) {
                echo '<div class="col-md-3 col-sm-12 col-xs-12" id="service-box2">';
                echo '<div class="post">';
                echo '<a href="' . get_the_permalink() . '">';
                echo '<div class="entry-content">';
                echo "<h4>" . get_the_title() . "</h4>";
                echo "</a>";
                echo "<p>" . get_the_excerpt() . "</p>";
                echo "</div>";
                echo "</div>";
                echo '<div class="learn">';
                echo '<a href="' . get_the_permalink() . '">';
                echo "LEARN MORE";
                echo "</a>";
                echo "</div>";
                echo "</div>";
                $counter++;
            } else {
                break;
            }
        endwhile;
        $post = $current_post;
        global $post;
        $current_post = $post;
        if ($counter < 4) {
            $left_overs = 4 - $counter;
            $getPosts = new WP_Query(array('showposts' => $left_overs, 'orderby' => 0));
            if ($getPosts->have_posts()):
                while ($getPosts->have_posts()):
                    $getPosts->the_post();
                    echo '<div class="col-md-3 col-sm-12 col-xs-12" id="service-box2">';
                    echo '<div class="post">';
                    echo '<a href="' . get_the_permalink() . '">';
                    echo '<div class="entry-content">';
                    echo "<h4>" . get_the_title() . "</h4>";
                    echo "</a>";
                    echo "<p>" . get_the_excerpt() . "</p>";
                    echo "</div>";
                    echo "</div>";
                    echo '<div class="learn">';
                    echo '<a href="' . get_the_permalink() . '">';
                    echo "LEARN MORE";
                    echo "</a>";
                    echo "</div>";
                    echo "</div>";
                endwhile;
            endif;
        }
        ?>
    </div>
    </div>

    <!--/End of Post blocks-->
    <?php
    get_footer();