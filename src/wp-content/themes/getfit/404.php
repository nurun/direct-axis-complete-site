<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package blueamber
 */

get_header();
?>
    <section>
        <div class="container">
            <div class="row">
                <!--Blog Posts-->
                <div class="col-md-12 col-xs-12">
                    <div class="site-content">
                        <h1 style="
    text-align: center;    font-size: 60px;
">Error 404</h1>
                        <p style="text-align:Center">Oops Page Not Found</p>
                        <div style="clear:both"></div>

                        <div class="learn" style="position: inherit;
    bottom: 60px;
    left: 125px;
    display: block;
    margin: 0 auto;"><a href="/" style="    margin: 0 auto;
    width: 100px;
    margin-top: 30px;display:block">Go Back</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
