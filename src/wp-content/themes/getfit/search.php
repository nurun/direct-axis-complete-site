<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package blueamber
 */

get_header();
?>
<h1>Search</h1>
<?php
get_footer();
