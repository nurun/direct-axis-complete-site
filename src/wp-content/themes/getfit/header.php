<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <script>
        var dataLayer = [{}];
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5KSZHQ4');
    </script>
    <!-- End Google Tag Manager -->
    <title><?php wp_title(''); ?></title>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php if (is_single()) : ?>
        <meta property="og:title" content="<?php wp_title(''); ?>"/>
        <meta property="og:description"
              content="<?php echo wp_strip_all_tags(get_the_excerpt());?>"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?php echo get_permalink();?>"/>
        <meta property="og:image:secure_url"
              content="<?php echo get_field('desktop_infographic', get_the_ID()); ?>"/>
        <meta property="og:image"
              content="<?php echo get_field('desktop_infographic', get_the_ID()); ?>"/>
    <?php else : ?>
        <meta property="og:title" content="Get Financially Fit"/>
        <meta property="og:description"
              content="I just passed a financial fitness test and could win R15 000 towards my savings fund. How financially fit do you think you are? Take the test here. #getfinaciallyfit"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?php echo get_permalink();?>"/>
        <meta property="og:image:secure_url"
              content="<?php echo get_template_directory_uri() . '/assets/images/fb_share.jpg'; ?>"/>
        <meta property="og:image"
              content="<?php echo get_template_directory_uri() . '/assets/images/fb_share.jpg'; ?>"/>
    <?php endif; ?>

    <?php if (is_singular() && pings_open(get_queried_object())) : ?>


        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,800" rel="stylesheet">

    <?php wp_head();
    ?>

</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5KSZHQ4"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo home_url('/'); ?>" class="brand"><img
                        alt="<?php bloginfo("name"); ?>"
                        src="<?php echo get_template_directory_uri() . '/assets/images/logo.jpg'; ?>"
                        alt="<?php bloginfo("name"); ?>"
                        class="logo_image" style="width:160px"></a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        </div>
    </div>
</nav>
<!-- End of Navbar -->
