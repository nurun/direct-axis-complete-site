<?php
/*
Template Name: Front Page
*/
get_header();
$postID = get_the_ID();
?>
    <section id="home">
<?php
/*
 * Hero Banner
 * */
get_template_part('template-parts/content-hero-banner-homepage');
?>
    <section id="section" class="service">
        <div class="container">

            <?php
            $count = 1;
            $args = array(
                'post_type' => 'post'
            );

            $post_query = new WP_Query($args);
            if ($post_query->have_posts()) {
                while ($post_query->have_posts()) {
                    $post_query->the_post();
                    if ($count == 1 || $count == 4) :
                        echo '<div class="row busiprof-features-content">';
                    endif;
                    ?>


                    <div class="col-md-4 col-sm-6 col-xs-12 service-box">
                        <div class="post">

                            <a href="<?php the_permalink(); ?>">
                                <div class="entry-header">
                                    <h4 class="entry-title"><?php the_title(); ?></h4>
                                </div>
                            </a>
                            <div style="clear:both">
                                <p><?php echo get_the_excerpt(); ?></p>
                            </div>
                            <div class="learn">
                                <a href="<?php the_permalink(); ?>">LEARN MORE</a>
                            </div>
                        </div>
                    </div> <!-- col -->

                    <?php if ($count == 3) :
                        echo '</div>';
                    endif;
                    $count++;

                }
            }
            ?>

        </div>
        </div>
    </section>
<?php
get_footer();
