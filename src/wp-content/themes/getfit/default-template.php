<?php
/*
Template Name: Content Page
*/
get_header();
?>
    <!-- Content -->
    <section>
        <div class="container">
            <div class="row">
                <!--Blog Posts-->
                <div class="col-md-12 col-xs-12">
                    <div class="site-content">
                        <?php
                        if (have_posts()) :
                            // Start the Loop.
                            while (have_posts()) : the_post();
                                the_content();
                            endwhile;
                            ?>
                        <?php endif; ?>
                    </div>
                    <!--/End of Blog Posts-->
                </div>
            </div>
        </div>
    </section>
    <!-- End of Blog & Sidebar Section -->

    <div class="clearfix"></div>
<?php get_footer(); ?>