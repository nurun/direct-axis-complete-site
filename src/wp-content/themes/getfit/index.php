<?php
get_header();
the_post();
?>
    <section id="default">

        <?php
        /*
         * Hero Banner
         * */
        get_template_part('template-parts/content-hero-banner-page');
        ?>

        <div class="container">
        <div class="row">
            <?php

            query_posts('posts_per_page=5');

            // The Loop
            while (have_posts()) : the_post();
                ?>
                <div class="post-box col-l-3 col-m-6 col-s-12">
                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>"/>
                    <p><?php the_date('d M y');?></p>
                    <?php the_title('<a href="'.get_the_permalink().'">', '</a>'); ?>
                    <div class="content"><a href="<?php the_permalink(); ?>">VIEW ARTICLE</a></div>
                </div>

                <?php
            endwhile; ?>
        </div>
        </div>
    </section>
<?php
get_footer();
