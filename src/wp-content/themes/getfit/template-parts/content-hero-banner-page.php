<div id="main" role="main">
    <section class="slider">
        <ul class="slides">
            <li>
                <?php if (wp_is_mobile()) { ?>
                    <img alt="img" src="<?php echo get_field('mobile_banner'); ?>" class="headerimage" width="100%"/>
                <?php } else { ?>
                    <img alt="img" src="<?php echo get_field('desktop_banner'); ?>" class="headerimage" width="100%"/>
                <?php } ?>
            </li>
        </ul>
    </section>
</div>
<!-- End of Slider -->
<div class="clearfix"></div>
