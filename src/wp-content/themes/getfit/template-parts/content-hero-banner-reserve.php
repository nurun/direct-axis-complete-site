<div class="wrapper">
    <div class="row">
        <div class="col-12">
            <div class="mainimage"
                 style="background: url('<?php the_field('banner_image'); ?>');    background-size: cover; background-repeat: no-repeat;">
            </div>
            <div class="imageoverlay"></div>
            <div id="textoverlay">
                <h1><?php the_field('banner_title'); ?></h1>
                <p>Blue Amber Zanzibar is a hyper-luxury tropical community that will uniquely blend sophistication and
                    lifestyle with global leaders in hospitality and world-class facilities to create a new ideal in
                    Equatorial Living. An unimaginable reality, made real.</p>
            </div>
        </div>
    </div>
    <div class="row grey">
        <div class="container">
            <div class="col-12">
                <div class="reservelink reserveform"><?php echo do_shortcode('[contact-form-7 id="140" title="Reserve you place"]'); ?></div>
            </div>
        </div>
    </div>
</div>