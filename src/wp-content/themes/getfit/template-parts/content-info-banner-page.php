<?php
if (have_rows('info_banners')) :
    while (have_rows('info_banners')) : the_row();
        ?>
        <div class="row">
            <div class="col-12 info_banner" style="background-image: url('<?php the_sub_field('banner_image'); ?>');">
                <div class="col-12 banner">
                    <div class="bannertext">
                        <h2><?php the_sub_field('banner_title'); ?></h2>
                        <?php the_sub_field('banner_copy'); ?>
                        <a href="<?php the_sub_field('banner_link'); ?>"><?php the_sub_field('banner_link_text')?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile;
endif;