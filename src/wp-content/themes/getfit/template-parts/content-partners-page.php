<?php
if (have_rows('partners')) :
?>
<div class="row grey">
    <div class="container">
        <div class="col-12">
            <h2 class="center blue">Hotel partners</h2>
            <?php
            while (have_rows('partners')) : the_row();
                ?>
                <div class="partners">
                    <div class="col-l-4 col-m-4 col-s-12 scroller" style="background: url('<?php echo get_sub_field('main_image'); ?>'); background-size: cover; background-repeat: no-repeat; margin-left: 2%; >
                        <div class="partneroverlay">
                            <img src="<?php echo get_sub_field('logo'); ?>">
                            <p><a href="<?php echo get_sub_field('partner_link'); ?>" class="content">Visit this hotel</a>
                            </p>
                        </div>
                    </div>
                </div>


            <?php
            endwhile;
            endif;

            ?>
        </div>
    </div>
</div>