<?php
if (get_field('gallery_title')) :
    ?>


        <div class="row" style="padding: 40px 0">
            <div class="col-12">
                <ul class="pgwSlider">
                    <h3><?php the_field('gallery_title') ?></h3>

                    <?php
                    if (have_rows('gallery_item')) :
                        while (have_rows('gallery_item')) : the_row();
                            ?>
                            <li><img src="<?php the_sub_field('image') ?>" alt="<?php the_sub_field('title') ?>"
                                     data-description="<?php the_sub_field('description') ?>">
                            </li>

                        <?php endwhile;
                    endif; ?>
                </ul>
            </div>
        </div>

<?php
endif;