<div class="wrapper grey">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="content">
                    <?php if (get_field('intro_title')) : ?>
                        <h2 class="center"><?php the_field('intro_title'); ?></h2>
                    <?php endif; ?>
                    <div class="col-l-6 col-m-12 col-s-12 image">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="col-l-6 col-m-12 col-s-12">
                        <?php the_post();
                        the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
