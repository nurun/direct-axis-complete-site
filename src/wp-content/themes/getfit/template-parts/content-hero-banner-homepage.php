<?php $detect = new Mobile_Detect(); ?>
<?php if (get_field('desktop_banner')) : ?>
    <div id="main" role="main">
        <section id="section" class="service">
            <div class="container">
                <div class="row busiprof-features-content">
                    <div class="col-xs-12 service-box">
                        <section class="slider">
                            <ul class="slides">
                                <li><a href="/quiz">
                                        <?php if ($detect->isMobile() && get_field('mobile_banner')) { ?>
                                            <img alt="img" src="<?php echo get_field('mobile_banner'); ?>"
                                                 class="headerimage"
                                                 width="100%"/>
                                        <?php } elseif ($detect->isTablet() && get_field('tablet_banner')) { ?>
                                            <img alt="img" src="<?php echo get_field('tablet_banner'); ?>"
                                                 class="headerimage"
                                                 width="100%"/>
                                        <?php } else { ?>
                                            <img alt="img" src="<?php echo get_field('desktop_banner'); ?>"
                                                 class="headerimage"
                                                 width="100%"/>
                                        <?php } ?>
                                    </a></li>
                            </ul>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php endif; ?>
<!-- End of Slider -->
<div class="clearfix"></div>
